# README #

### Background ###

* This is the website code of Gender Statistics. The goal of this project is to create a new interactive website to replace the [old Gender Statistics website](http://genderstats.un.org/)
* Version: 1.3
* Demos:
    - [Development demo by APIS](https://unite.un.org/sites/unite.un.org/files/app-desa-genderstats/index.html#/home)
    - [Demo hosted by DESA](http://genderstats.un.org/beta/index.html#/home)
    - [Demo hosted by Qlik](http://webapps.qlik.com/un/gender-statistics/index.html#/home)
* Technology used: [Angular.js](https://angularjs.org/), Javascript, [QlikSense API](https://help.qlik.com/en-US/sense-developer/3.0/Content/api-version-history.htm)

The Backend database for this Qlik app is here:

https://viz.unite.un.org/visualization/single/?appid=63172c31-bf2d-4083-bc50-eb19e14978b5&sheet=VPEzmdE&select=clearall

### How do I get set up? ###

* Steps
    - Down load the repository by clicking the Download button at the left menu at bitbucket.org

    - Use a web server to run your webpages locally

* Configuration (check these files):
In the folder: \js\lib\

Open the file main.js
Look for these variables and enter the relevant info:

        - `scriptsUrl` 
        - `baseUrl`

Open the file app.js
Look for the variable:

        - `me.config`


### Questions ###

- Developed by Qlik:
	- Yianni Ververis
	- Jennell McIntire

- With contributions from OICT: 
	- Daniela Marin Puentes, 
	- Kania Azrina, 
	- Yilin Wei, 
	- Jorge Martinez Navarrete

- Business Owners at UN-DESA
	- Haoyi Chen
	- Andrew Smith